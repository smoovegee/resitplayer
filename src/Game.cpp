/*-------------------------------------------------------------------------
Significant portions of this project are based on the Ogre Tutorials
- https://ogrecave.github.io/ogre/api/1.10/tutorials.html
Copyright (c) 2000-2013 Torus Knot Software Ltd

Manual generation of meshes from here:
- http://wiki.ogre3d.org/Generating+A+Mesh

*/

#include <exception>
#include <iostream>
#include "OgreImGuiInputListener.h"
#include "OgreApplicationContextBase.h"
#include "OgreInput.h"
#include "OgreStringConverter.h"

#include <OgreSceneLoaderManager.h>
//#include "OgreDotScenePlugin.h"

#include "Game.h"


//std::deque<Ogre::Vector3> mWalkList;


std::deque<Ogre::Vector3> mWalkList;
Game::Game() : ApplicationContext("OgreTutorialApp")
{
    dynamicsWorld = NULL;

    wDown = false;
    aDown = false;
	sDown = false;
	dDown = false;
	
	this->mBoxAnimationState = 0;
 

}



Game::~Game()
{
	
    //cleanup in the reverse order of creation/initialization

    ///-----cleanup_start----
    //remove the rigidbodies from the dynamics world and delete them

    for (int i = dynamicsWorld->getNumCollisionObjects() - 1; i >= 0; i--)
    {
      btCollisionObject* obj = dynamicsWorld->getCollisionObjectArray()[i];
    	btRigidBody* body = btRigidBody::upcast(obj);

    	if (body && body->getMotionState())
    	{
    		delete body->getMotionState();
    	}

    	dynamicsWorld->removeCollisionObject(obj);
    	delete obj;
    }

    //delete collision shapes
    for (int j = 0; j < collisionShapes.size(); j++)
    {
    	btCollisionShape* shape = collisionShapes[j];
    	collisionShapes[j] = 0;
    	delete shape;
    }

    //delete dynamics world
    delete dynamicsWorld;

    //delete solver
    delete solver;

    //delete broadphase
    delete overlappingPairCache;

    //delete dispatcher
    delete dispatcher;

    delete collisionConfiguration;

    //next line is optional: it will be cleared by the destructor when the array goes out of scope
    collisionShapes.clear();
}


void Game::setup()
{
    // do not forget to call the base first
    ApplicationContext::setup();

    addInputListener(this);

    // get a pointer to the already created root
    Root* root = getRoot();
    scnMgr = root->createSceneManager();


    // register our scene with the RTSS
    RTShader::ShaderGenerator* shadergen = RTShader::ShaderGenerator::getSingletonPtr();
    shadergen->addSceneManager(scnMgr);

    bulletInit();

    setupCamera();

    setupFloor();
   
    loadScene();

    setupLights();
    setupBox2();

    //setupBoxMesh2();

    setupPlayer();
	setupBox3();
	setupBoxMesh();
	setupBoxAnimation();

	setupNinjaMesh();
	setupNinjaAnimation();
	
}


void Game::setupBox2() {

	// WHen this bit it added. it crashes the game for some reason.
	//AnimationState->setLoop(true);
	//AnimationState->setEnabled(true);
		//float Distance(0);
		//float WalkSpd(70.0);
		//float Node(0);
		//Direction = Ogre::Vector3::ZERO;

	Entity* box2 = scnMgr->createEntity("cube.mesh");
	box2->setMaterialName("Examples/spheremap"); // was able to add different images to objects thats great
	SceneNode* boxSceneNode = scnMgr->getRootSceneNode()->createChildSceneNode(Vector3(84, 48, 0));
	boxSceneNode->attachObject(box2);
	boxSceneNode->setScale(1.0f, 1.0f, 1.0f);
	boxSceneNode->_updateBounds();
	const AxisAlignedBox& b = boxSceneNode->_getWorldAABB();
	Vector3 temp(b.getSize());
	meshBoundingBox = temp;
	boundingBoxFromOgre();

	///can we add collision to this box?
	Vector3 axis(1.0, 1.0, 0.0);
	axis.normalise();
	Radian rads(Degree(60.0));
	Quaternion quat(rads, axis);
	boxSceneNode->setOrientation(quat);
	boxSceneNode->setPosition(-200, 0, 0);
	


	Vector3 meshBoundingBox(b.getSize());

	if (meshBoundingBox == Vector3::ZERO)
	{
		std::cout << "bounding voluem size is zero." << std::endl;
	}

	//create a dynamic rigidbody

	btCollisionShape* colShape = new btBoxShape(btVector3(meshBoundingBox.x / 2.0f, meshBoundingBox.y / 2.0f, meshBoundingBox.z / 2.0f));
	std::cout << "Mesh box col shape [" << (float)meshBoundingBox.x << " " << meshBoundingBox.y << " " << meshBoundingBox.z << "]" << std::endl;
	// btCollisionShape* colShape = new btBoxShape(btVector3(10.0,10.0,10.0));
	 //btCollisionShape* colShape = new btSphereShape(btScalar(1.));
	collisionShapes.push_back(colShape);

	/// Create Dynamic Objects
	btTransform startTransform;
	startTransform.setIdentity();

	//startTransform.setOrigin(btVector3(0, 200, 0));

	Quaternion quat2 = boxSceneNode->_getDerivedOrientation();
	startTransform.setRotation(btQuaternion(quat2.x, quat2.y, quat2.z, quat2.w));

	Vector3 pos = boxSceneNode->_getDerivedPosition();
	startTransform.setOrigin(btVector3(pos.x, pos.y, pos.z));



	btScalar mass(1.f);

	//rigidbody is dynamic if and only if mass is non zero, otherwise static
	bool isDynamic = (mass != 0.f);

	btVector3 localInertia(0, 0, 0);
	if (isDynamic)
	{
		// Debugging
		//std::cout << "I see the cube is dynamic" << std::endl;
		colShape->calculateLocalInertia(mass, localInertia);
	}

	std::cout << "Local inertia [" << (float)localInertia.x() << " " << localInertia.y() << " " << localInertia.z() << "]" << std::endl;

	//using motionstate is recommended, it provides interpolation capabilities, and only synchronizes 'active' objects
	btDefaultMotionState* myMotionState = new btDefaultMotionState(startTransform);
	btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, myMotionState, colShape, localInertia);
	btRigidBody* body = new btRigidBody(rbInfo);

	//Link to ogre
	body->setUserPointer((void*)boxSceneNode);

	body->setRestitution(0.5);

	dynamicsWorld->addRigidBody(body);


	//More Entities
	//More Entities

	ent = scnMgr->createEntity("knot.mesh");
	Node = scnMgr->getRootSceneNode()->createChildSceneNode(Ogre::Vector3(200, 0.0, -25.0));
	ent->setMaterialName("Examples/TudorHouse");
	Node->attachObject(ent);
	Node->setScale(1.1, 1.1, 1.1);

	ent = scnMgr->createEntity("knot.mesh");
	Node = scnMgr->getRootSceneNode()->createChildSceneNode(Ogre::Vector3(-550.0, 0.0, -100.0));
	ent->setMaterialName("Examples/rockwall");
	Node->attachObject(ent);
	Node->setScale(1.1, 1.1, 1.1);

	ent = scnMgr->createEntity("knot.mesh");
	Node = scnMgr->getRootSceneNode()->createChildSceneNode(Ogre::Vector3(-100.0, 0.0, -500.0));
	ent->setMaterialName("Examples/GreenSkin");
	Node->attachObject(ent);
	Node->setScale(1.1, 1.1, 1.1);




}
void Game::boundingBoxFromOgre()
{
	//get bounding box here.
	Vector3 meshBoundingBox(0.0f, 0.0f, 0.0f);
	
}
void Game::setupRobot() {
	
	//More Entities	
}

void Game::setupBox3() {



	Entity* box3 = scnMgr->createEntity("tudorhouse.mesh");
	box3->setMaterialName("Examples/TudorHouse"); // was able to add different images to objects thats great
	SceneNode* boxSceneNode = scnMgr->getRootSceneNode()->createChildSceneNode(Vector3(84, 48, 0));
	boxSceneNode->attachObject(box3);
	boxSceneNode->setScale(0.3f, 0.3f, 0.3f);
	boxSceneNode->_updateBounds();
	boxSceneNode->setPosition(350, 0, 0);
	const AxisAlignedBox& b = boxSceneNode->_getWorldAABB();
	Vector3 temp(b.getSize());
	meshBoundingBox = temp;
	boundingBoxFromOgre();

	///can we add collision to this box?
	Vector3 axis(1.0, 1.0, 0.0);
	axis.normalise();
	Radian rads(Degree(0.0));
	Quaternion quat(rads, axis);
	boxSceneNode->setOrientation(quat);
	//boxSceneNode->setPosition(0, 0, 0);


	Vector3 meshBoundingBox(b.getSize());

	if (meshBoundingBox == Vector3::ZERO)
	{
		std::cout << "bounding voluem size is zero." << std::endl;
	}

	//create a dynamic rigidbody

	btCollisionShape* colShape = new btBoxShape(btVector3(meshBoundingBox.x / 2.0f, meshBoundingBox.y / 2.0f, meshBoundingBox.z / 2.0f));
	std::cout << "Mesh box col shape [" << (float)meshBoundingBox.x << " " << meshBoundingBox.y << " " << meshBoundingBox.z << "]" << std::endl;
	// btCollisionShape* colShape = new btBoxShape(btVector3(10.0,10.0,10.0));
	 //btCollisionShape* colShape = new btSphereShape(btScalar(1.));
	collisionShapes.push_back(colShape);

	/// Create Dynamic Objects
	btTransform startTransform;
	startTransform.setIdentity();

	//startTransform.setOrigin(btVector3(0, 200, 0));

	Quaternion quat2 = boxSceneNode->_getDerivedOrientation();
	startTransform.setRotation(btQuaternion(quat2.x, quat2.y, quat2.z, quat2.w));

	Vector3 pos = boxSceneNode->_getDerivedPosition();
	startTransform.setOrigin(btVector3(pos.x, pos.y, pos.z));



	btScalar mass(1.f);

	//rigidbody is dynamic if and only if mass is non zero, otherwise static
	bool isDynamic = (mass != 0.f);

	btVector3 localInertia(0, 0, 0);
	if (isDynamic)
	{
		// Debugging
		//std::cout << "I see the cube is dynamic" << std::endl;
		colShape->calculateLocalInertia(mass, localInertia);
	}

	std::cout << "Local inertia [" << (float)localInertia.x() << " " << localInertia.y() << " " << localInertia.z() << "]" << std::endl;

	//using motionstate is recommended, it provides interpolation capabilities, and only synchronizes 'active' objects
	btDefaultMotionState* myMotionState = new btDefaultMotionState(startTransform);
	btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, myMotionState, colShape, localInertia);
	btRigidBody* body = new btRigidBody(rbInfo);

	//Link to ogre
	body->setUserPointer((void*)boxSceneNode);

	body->setRestitution(0.5);

	dynamicsWorld->addRigidBody(body);



}

void Game::setupNinjaMesh()
{
	// Try and load in one of the ogre test meshes.
	try
	{
		ninjaEntity = scnMgr->createEntity("ninja.mesh");
		ninjaEntity->setCastShadows(true);
		ninjaSceneNode = scnMgr->getRootSceneNode()->createChildSceneNode();
		ninjaSceneNode->attachObject(ninjaEntity);
		ninjaSceneNode->setPosition(0, 0, 300);




	}
	catch (Exception e)
	{
		e.what();
	}
	mWalkList.push_back(Ogre::Vector3(550.0, 0, 50.0));
	mWalkList.push_back(Ogre::Vector3(-100.0, 0, -200.0));
	mWalkList.push_back(Ogre::Vector3(0, 0, 25.0));
}

void Game::setupBoxMesh()
{



	box = scnMgr->createEntity("cube.mesh");
	box->setCastShadows(true);

	boxNode = scnMgr->getRootSceneNode()->createChildSceneNode();
	boxNode->attachObject(box);

	boxNode->setPosition(0, 0, 0);
	boxNode->setScale(0.5, 0.5, 0.5);

}



void Game::setupNinjaAnimation()
{

	mNinjaAnimationState = ninjaEntity->getAnimationState("Idle3");
	mNinjaAnimationState->setLoop(true);
	mNinjaAnimationState->setEnabled(true);
}

void Game::setupBoxAnimation()
{
	//this is what got the box moving
	Real duration = 4.0;
	Real step = duration / 4.0;
	Real x = 100, y = 50, z = 100;
	Real s = 0.5f;

	boxNode->setPosition(x, y, z);
	boxNode->setScale(s, s, s);

	Animation* animation = scnMgr->createAnimation("BoxAnim", duration);
	animation->setInterpolationMode(Animation::IM_SPLINE);
	NodeAnimationTrack* track = animation->createNodeTrack(0, boxNode);

	TransformKeyFrame* key;
//this made the box go in a circle
	key = track->createNodeKeyFrame(0.0f);
	key->setTranslate(Vector3(-x, y, -z));
	key->setScale(Vector3(s, s, s));

	key = track->createNodeKeyFrame(step);
	key->setTranslate(Vector3(-x, y, z));
	key->setScale(Vector3(s, s, s));

	key = track->createNodeKeyFrame(2.0*step);
	key->setTranslate(Vector3(x, y, z));
	key->setScale(Vector3(s, s, s));

	key = track->createNodeKeyFrame(3.0*step);
	key->setTranslate(Vector3(x, y, -z));
	key->setScale(Vector3(s, s, s));

	key = track->createNodeKeyFrame(4.0*step);
	key->setTranslate(Vector3(-x, y, -z));
	key->setScale(Vector3(s, s, s));

	mBoxAnimationState = scnMgr->createAnimationState("BoxAnim");
	mBoxAnimationState->setEnabled(true);
	mBoxAnimationState->setLoop(true);
}
bool Game::frameRenderingQueued(const FrameEvent& evt)
{
	//method in parent is virtual and is called anyway

	//update the animation by the time before the last frame.
	//mBoxAnimationState->addTime(evt.timeSinceLastFrame);

	mNinjaAnimationState->addTime(evt.timeSinceLastFrame);
	mBoxAnimationState->addTime(evt.timeSinceLastFrame);
	//this is very important or it doesnt move
	return true;
}


//need the proper coordinates for this
void Game::setupSkyBox() {
	//this doesnt work cause i am not able to loook up
	Ogre::Plane plane;
	plane.d = 1000;
	plane.normal = Ogre::Vector3::NEGATIVE_UNIT_Y;
	scnMgr->setSkyPlane(true, plane, "Examples/Water02", 1500, 50, true, 1.5, 150, 150);



}
void Game::setupCamera()
{
    // Create Camera
    Camera* cam = scnMgr->createCamera("myCam");

    //Setup Camera
    cam->setNearClipDistance(10);

    // Position Camera - to do this it must be attached to a scene graph and added
    // to the scene.
    SceneNode* camNode = scnMgr->getRootSceneNode()->createChildSceneNode();
	camNode->setPosition(90.0, 280.0, 800.0);//50, 400,1200);
	camNode->pitch(Ogre::Degree(-30.0));
	camNode->yaw(Ogre::Degree(-15.0));
    camNode->lookAt(Vector3(0, 0, 0), Node::TransformSpace::TS_WORLD);
    camNode->attachObject(cam);

	

    // Setup viewport for the camera.
    Viewport* vp = getRenderWindow()->addViewport(cam);
    vp->setBackgroundColour(ColourValue(0, 0, 0));

    // link the camera and view port.
    cam->setAspectRatio(Real(vp->getActualWidth()) / Real(vp->getActualHeight()));


}

void Game::bulletInit()
{
    ///collision configuration contains default setup for memory, collision setup. Advanced users can create their own configuration.
    collisionConfiguration = new btDefaultCollisionConfiguration();

     ///use the default collision dispatcher. For parallel processing you can use a diffent dispatcher (see Extras/BulletMultiThreaded)
     dispatcher = new btCollisionDispatcher(collisionConfiguration);

     ///btDbvtBroadphase is a good general purpose broadphase. You can also try out btAxis3Sweep.
     overlappingPairCache = new btDbvtBroadphase();

     ///the default constraint solver. For parallel processing you can use a different solver (see Extras/BulletMultiThreaded)
     solver = new btSequentialImpulseConstraintSolver;

     dynamicsWorld = new btDiscreteDynamicsWorld(dispatcher, overlappingPairCache, solver, collisionConfiguration);

     dynamicsWorld->setGravity(btVector3(0, -10, 0));
}



void Game::setupFloor()
{
    // Create a plane
    Plane plane(Vector3::UNIT_Y, 0);

    // Define the plane mesh
    MeshManager::getSingleton().createPlane(
            "ground", RGN_DEFAULT,
            plane,
            1500, 1500, 20, 20,
            true,
            1, 5, 5,
            Vector3::UNIT_Z);

    // Create an entity for the ground
    Entity* groundEntity = scnMgr->createEntity("ground");

	//another Floor?

	//this does work either.
	// Define the plane mesh
	MeshManager::getSingleton().createPlane(
		"wall", RGN_DEFAULT,
		plane,
		1500, 1500, 20, 20,
		true,
		20,20,20,
		Vector3::UNIT_X);

	// Create an entity for the ground
	Entity* wallEntity = scnMgr->createEntity("wall");
	wallEntity->setMaterialName("Examples/Water01");
	
    //Setup ground entity
    // Shadows off
    groundEntity->setCastShadows(false);

    // Material - Examples is the rsources file,
    // Rockwall (texture/properties) is defined inside it.
	groundEntity->setMaterialName("Examples/Water01");

    // Create a scene node to add the mesh too.
    SceneNode* thisSceneNode = scnMgr->getRootSceneNode()->createChildSceneNode();
    thisSceneNode->attachObject(groundEntity);

    //the ground is a cube of side 100 at position y = 0.
	   //the sphere will hit it at y = -6, with center at -5
    btCollisionShape* groundShape = new btBoxShape(btVector3(btScalar(1500.), btScalar(50.), btScalar(1500.)));

    collisionShapes.push_back(groundShape);

    btTransform groundTransform;
    groundTransform.setIdentity();
  //  groundTransform.setOrigin(btVector3(0, -100, 0));

    Vector3 pos = thisSceneNode->_getDerivedPosition();

    //Box is 100 deep (dimensions are 1/2 heights)
    //but the plane position is flat.
    groundTransform.setOrigin(btVector3(pos.x, pos.y-50.0, pos.z));

    Quaternion quat2 = thisSceneNode->_getDerivedOrientation();
    groundTransform.setRotation(btQuaternion(quat2.x, quat2.y, quat2.z, quat2.w));


    btScalar mass(0.0f);

    //rigidbody is dynamic if and only if mass is non zero, otherwise static
    bool isDynamic = (mass != 0.f);

    btVector3 localInertia(0, 0, 0);
    if (isDynamic)
        groundShape->calculateLocalInertia(mass, localInertia);

    //using motionstate is optional, it provides interpolation capabilities, and only synchronizes 'active' objects
    btDefaultMotionState* myMotionState = new btDefaultMotionState(groundTransform);
    btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, myMotionState, groundShape, localInertia);
    btRigidBody* body = new btRigidBody(rbInfo);

    //   body->setRestitution(0.0);

    //add the body to the dynamics world
    dynamicsWorld->addRigidBody(body);
}



bool Game::frameStarted (const Ogre::FrameEvent &evt)
{
  //Be sure to call base class - otherwise events are not polled.
	ApplicationContext::frameStarted(evt);
	//AnimationState* mSinbadAnimationState;
	//mSinbadAnimationState->addTime(evt.timeSinceLastFrame);
	

  if (this->dynamicsWorld != NULL)
  {
      // Apply new forces
      if(wDown)
        player->forward();

      if(sDown)
      {
        player->backward();
        //player->spinRight(); // -- Doesnt' work
      }

	  if (aDown)
	  {
		  player->turnLeft();
	  }

	  if (dDown)
	  {
		  player->turnRight();
	  }
	
      // Bullet can work with a fixed timestep
      //dynamicsWorld->stepSimulation(1.f / 60.f, 10);

      // Or a variable one, however, under the hood it uses a fixed timestep
      // then interpolates between them.

     dynamicsWorld->stepSimulation((float)evt.timeSinceLastFrame, 10);

     // update positions of all objects
     for (int j = dynamicsWorld->getNumCollisionObjects() - 1; j >= 0; j--)
     {
         btCollisionObject* obj = dynamicsWorld->getCollisionObjectArray()[j];
         btRigidBody* body = btRigidBody::upcast(obj);
         btTransform trans;

         if (body && body->getMotionState())
         {
            body->getMotionState()->getWorldTransform(trans);

            /* https://oramind.com/ogre-bullet-a-beginners-basic-guide/ */
            void *userPointer = body->getUserPointer();

            // Player should know enough to update itself.
            if(userPointer == player)
            {
                // Ignore player, he's always updated!
            }
            else //This is just to keep the other objects working.
            {
              if (userPointer)
              {
                btQuaternion orientation = trans.getRotation();
                Ogre::SceneNode *sceneNode = static_cast<Ogre::SceneNode *>(userPointer);
                sceneNode->setPosition(Ogre::Vector3(trans.getOrigin().getX(), trans.getOrigin().getY(), trans.getOrigin().getZ()));
                sceneNode->setOrientation(Ogre::Quaternion(orientation.getW(), orientation.getX(), orientation.getY(), orientation.getZ()));
              }
            }
          }
          else
          {
            trans = obj->getWorldTransform();
          }
		 //mAnimationState->addTime((float)evt.timeSinceLastFrame);
		
		 
     }

     //Update player here, his movement is not dependent on collisions.
	 player->update();

	
   }

  return true;

}

bool Game::frameEnded(const Ogre::FrameEvent &evt)
{
	if (this->dynamicsWorld != NULL)
	{
		// Bullet can work with a fixed timestep
		//dynamicsWorld->stepSimulation(1.f / 60.f, 10);

		// Or a variable one, however, under the hood it uses a fixed timestep
		// then interpolates between them.

		dynamicsWorld->stepSimulation((float)evt.timeSinceLastFrame, 10);


	}
	
	return true;
}

void Game::setupLights()
{
    // Setup Abient light
    scnMgr->setAmbientLight(ColourValue(0.7, 0.7, 0.7));
	SceneNode* ambientLightNode = scnMgr->getRootSceneNode()->createChildSceneNode();
	ambientLightNode->setPosition(Vector3(200, 800, -600));
    scnMgr->setShadowTechnique(ShadowTechnique::SHADOWTYPE_STENCIL_MODULATIVE);

    // Add a spotlight
    Light* spotLight = scnMgr->createLight("SpotLight");

    // Configure
    spotLight->setDiffuseColour(0, 0, 1.0);
   spotLight->setSpecularColour(0, 0, 1.0);
    spotLight->setType(Light::LT_SPOTLIGHT);
   


    // Create a schene node for the spotlight
    SceneNode* spotLightNode = scnMgr->getRootSceneNode()->createChildSceneNode();
    spotLightNode->setDirection(-1, -1, 0);
    spotLightNode->setPosition(Vector3(200, 200, 0));
	spotLight->setSpotlightRange(Degree(35), Degree(50));
    // Add spotlight to the scene node.
    spotLightNode->attachObject(spotLight);

    // Create directional light
    Light* directionalLight = scnMgr->createLight("DirectionalLight");

    // Configure the light
    directionalLight->setType(Light::LT_DIRECTIONAL);
    directionalLight->setDiffuseColour(ColourValue(0.4, 0, 0));
   directionalLight->setSpecularColour(ColourValue(0.4, 0, 0));

    // Setup a scene node for the directional lightnode.
    SceneNode* directionalLightNode = scnMgr->getRootSceneNode()->createChildSceneNode();
    directionalLightNode->attachObject(directionalLight);
    directionalLightNode->setDirection(Vector3(0, -1, 1));

    // Create a point light
    Light* pointLight = scnMgr->createLight("PointLight");

    // Configure the light
    pointLight->setType(Light::LT_POINT);
    pointLight->setDiffuseColour(0.3, 0.3, 0.3);
    pointLight->setSpecularColour(0.3, 0.3, 0.3);

    // setup the scene node for the point light
    SceneNode* pointLightNode = scnMgr->getRootSceneNode()->createChildSceneNode();

    // Configure the light
    pointLightNode->setPosition(Vector3(0, 600, -300));

    // Add the light to the scene.
    pointLightNode->attachObject(pointLight);


	//wanted to see if this lamp stuff worked and i cant really see anything.
	//Ogre::Light * lampLight = scnMgr->createLight("Lamp");
	//lampLight->setType(Light::LT_POINT);
	//lampLight->setDiffuseColour(0.3, 0.3, 0.3);
	//lampLight->setAttenuation(7, 1.0, 0.7, 1.7);
	//lampLight->setPosition(Ogre::Vector3(0.0, 0.0, 0.0));


}


//direction *= timeSinceLastFrame * speedMultiplier;
bool Game::keyPressed(const KeyboardEvent& evt)
{
	
    std::cout << "Got key down event" << std::endl;
    if (evt.keysym.sym == SDLK_ESCAPE)
    {
        getRoot()->queueEndRendering();
    }

    if(evt.keysym.sym == 'w')
    {
		wDown = true;
		
    }

    if(evt.keysym.sym == 'a')
    {
        aDown = true;
    }
	
	if (evt.keysym.sym == 's')
	{
		sDown = true;
	}

	if (evt.keysym.sym == 'd')
	{
		dDown = true;
	}

	
    return true;
}

bool Game::keyReleased(const KeyboardEvent& evt)
{
	
    std::cout << "Got key up event" << std::endl;

    if(evt.keysym.sym == 'w')
    {
        wDown = false;
		
    }

    if(evt.keysym.sym == 'a')
    {
        aDown = false;
    }

	if (evt.keysym.sym == 's')
	{
		sDown = false;
	}

	if (evt.keysym.sym == 'd')
	{
		dDown = false;
	}
    return true;
}




bool Game::mouseMoved(const MouseMotionEvent& evt)
{
	std::cout << "Got Mouse" << std::endl;
	return true;
}

void Game::loadScene()
{
	// Hacked version of the scene, all meshes replaced with cube.mesh. 
	//this works but need some time to position all objects
 // std::string sceneName("walls.scene");
  //Ogre::SceneLoaderManager::getSingleton().load(sceneName,ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,scnMgr->getRootSceneNode());
}

